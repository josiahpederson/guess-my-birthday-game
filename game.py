from calendar import month
from random import randint
from timeit import repeat

name = input("Hello, what is your name? ")
guess_list = []

for total in range(5):
    month_guess = randint(1, 12)
    year_guess = randint(1924, 2004)
    combined_guess = str(month_guess) + "/" + str(year_guess)
    guess_list.append(combined_guess)

    response = "none"

    while response != "no":
        print("Guess", total + 1, ":", name, "were you born in",
        month_guess, "/", year_guess, "?")

        response = input("yes or no? ")
        if response == "yes":
            print("I knew it!")
            print("Here were our guesses: ", guess_list)
            exit()
        elif response == "no":
            if total == 4:
                print("I have better things to do, tschuss!")
                print("Here were our guesses: ", guess_list)
                exit()
            else:
                print("Darn, lemme try again.")
        else:
            print("Learn to read! Invalid response. Try again.")